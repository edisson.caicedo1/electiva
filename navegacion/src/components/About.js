import React from 'react';

function About(){
    return(
        <div>
            <h2>Acerca de Nosotros</h2>
            <form>
                <div className='mb-3'>
                    <label className='form-label'>Nombres</label>
                    <input type='text' className='form-control' placeholder='Nombres'></input>
                </div>
                <div className='mb-3'>
                    <label className='form-label'>Correo electrónico</label>
                    <input type='email' className='form-control' placeholder='E-mail'></input>
                </div>
            </form>
        </div>
    );
}

export default About;