import React from 'react';

function Inicio(){
    return(
        <div>
            <h2>Inicio</h2>
            <table className='table'>
                <thead>
                    <tr>
                        <th>Encabezado 1</th>
                        <th>Encabezado 2</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Dato 1</td>
                        <td>Dato 2</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default Inicio;