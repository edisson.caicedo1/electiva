import React from 'react';

function Home(){
    return(
        <div>
            <h2>Inicio</h2>
            <table className='table'>
                <thead>
                    <tr>
                        <th>Encabezado 1</th>
                    </tr>
                    <tr>
                        <th>Encabezado 2</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Dato 1</td>
                        <td>Dato 2</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default Home;