import React from 'react';

function Contacto(){
    return(
        <div>
            <h2>Acerca de Nosotros</h2>
            <form>
                <div className='mb-3'>
                    <label className='form-label'>Nombres</label>
                    <input type='text' className='form-control' placeholder='Nombres'></input>
                </div>
                <div className='mb-3'>
                    <label className='form-label'>Correo electrónico</label>
                    <input type='email' className='form-control' placeholder='E-mail'></input>
                </div>
                <button type='submit' className='btn btn-success'>Enviar</button>
            </form>
        </div>
    );
}

export default Contacto;