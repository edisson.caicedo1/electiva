import React from 'react';
import { BrowserRouter as Router, Link, Routes, Route } from 'react-router-dom';
import Inicio from './components/Inicio';
import Contacto from './components/Contacto';
import Acerca from './components/Acerca';

function App(){
  return(
    <Router>
      <nav>
        <ul>
          <li>
            <Link to='/'>Inicio</Link>
          </li>
          <li>
            <Link to='/acerca'>Acerca de Nosotros</Link>
          </li>
          <li>
            <Link to='/contacto'>Contácto</Link>
          </li>
        </ul>
      </nav>
      <Routes>
        <Route path='/' element={<Inicio />} />
        <Route path='/acerca' element={<Acerca />} />
        <Route path='/contacto' element={<Contacto />} />
      </Routes>
    </Router>
  );
}
export default App;