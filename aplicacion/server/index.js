//Crear constante para importar la libreria express
const express = require("express");
//Aisgnación a una variable llamada app
const app = express();
//importar a una variable las funciones de mysql
const mysql = require ("mysql");

const cors = require("cors");

app.use(cors());
app.use(express.json());

//crear una constante con los datos de conexión
const db = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"electiva"
});
//recibir los datos enviados como mensaje .json
app.post("/create",(req,res)=>{
    const nombre = req.body.nombre;
    const edad = req.body.edad;
    const pais = req.body.cargo;
    const cargo = req.body.cargo;
//ejecutar la consulta a la base de datos
    db.query('INSERT INTO empleados (nombre, edad, pais, cargo) VALUES (?,?,?,?)',[nombre, edad, pais, cargo],
    (err,result)=>{
        if(err){
            console.log(err);
        }
        else{
            res.send("Empleado registrado correctamente")
        }
    }

    );
});


//Método para listar información
app.get("/empleados",(req,res)=>{
    //ejecutar la consulta a la base de datos
    db.query('SELECT * FROM empleados',
    (err,result)=>{
        if(err){
            console.log(err);
        }
        else{
            res.send(result);
        }
    }

    );
});

//Actualizar
app.put("/update",(req,res)=>{
    const id = req.body.id;
    const nombre = req.body.nombre;
    const edad = req.body.edad;
    const pais = req.body.cargo;
    const cargo = req.body.cargo;
//ejecutar la consulta a la base de datos
    db.query('UPDATE empleados SET nombre=?, edad=?, pais=?, cargo=? WHERE id=?',[nombre, edad, pais, cargo, id],
    (err,result)=>{
        if(err){
            console.log(err);
        }
        else{
            res.send("Empleado actualizado correctamente")
        }
    }
    );
});

//Eliminar
app.delete("/delete/:id", (req, res) => {
    const id = req.params.id;
  
    // Ejecutar la consulta a la base de datos
    db.query('DELETE FROM empleados WHERE id = ?', [id], (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Empleado eliminado correctamente.");
      }
    });
  });  


app.listen(3001, ()=>{
    console.log("Servidor ok, en el puerto 3001");
})