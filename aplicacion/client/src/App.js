import './App.css';
import {useState} from 'react';
import Axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'

function App() {
  const [nombre,setNombre] = useState("");
  const [edad,setEdad] = useState(0);
  const [pais,setPais] = useState("");
  const [cargo,setCargo] = useState("");
  const [id,setId] = useState("");

  const [editar, setEditar] = useState(false);

  const editarEmpleado = (val)=>{
    setEditar(true);

    setNombre(val.nombre);
    setEdad(val.edad);
    setPais(val.pais);
    setCargo(val.cargo);
    setId(val.id);
  };

  const [empleadosList,setEmpleados] = useState([])
//Insertar información
  const add = ()=>{
    Axios.post("http://localhost:3001/create",{
      nombre:nombre,
      edad:edad,
      pais:pais,
      cargo:cargo
    }).then(()=>{
      getEmpleados();
      limpiarCampos();
      Swal.fire({
        title:"<strong>Registro exitoso</strong>",
        html:"<i>El empleado" +nombre+ "fue registrado correctamente.</i>",
        icon: 'success'
      })
    });
  }
//Extraer información
  const getEmpleados = ()=>{
    Axios.get("http://localhost:3001/empleados").then((response)=>{
      setEmpleados(response.data);
    })
  }

  //actualizar información
  const update = ()=>{
    Axios.put("http://localhost:3001/update",{
      id:id,
      nombre:nombre,
      edad:edad,
      pais:pais,
      cargo:cargo
    }).then(()=>{
      getEmpleados();
      limpiarCampos();
      Swal.fire({
        title:"<strong>Actualización exitosa</strong>",
        html:"<i>El empleado" +nombre+ "fue actualizado correctamente.</i>",
        icon: 'success'
      })
    })
  }

//eliminar información
const eliminar = (id)=>{
  Axios.delete(`http://localhost:3001/delete/${id}`).then(()=>{
    getEmpleados();
    limpiarCampos();
    Swal.fire({
      title:"<strong>Eliminación exitosa</strong>",
      html:"<i>El empleado" +nombre+ "fue eliminado correctamente.</i>",
      buttons:["No","Si"],
      icon: 'warning'
    }).then((res)=>{
      if(res){

      }
    });
  })
}


  //Limpiar campos
  const limpiarCampos = ()=>{
    setNombre("");
    setEdad("");
    setPais("");
    setCargo("");
    setId("");
    setEditar(false);
  }

  //getEmpleados();

  return (
    <div className='container'>
    <div className="App">
        <div className="card">
          <div className="card-header">
            Gestión de clientes
          </div>
          <div className="card-body">
            <div className="datos">
              <div className="mb-3">
              <label className='form-label'>Nombre: <input type="text" className='form-control' value={nombre} onChange={(event)=>{
                setNombre(event.target.value);
              }} /> </label>
              </div>
              <div className="mb-3">
              <label className='form-label'>Edad: <input type="text" className='form-control' value={edad} onChange={(event)=>{
                setEdad(event.target.value);
              }} /> </label>
              </div>
              <div className="mb-3">
              <label className='form-label'>País: <input type="text" className='form-control' value={pais} onChange={(event)=>{
                setPais(event.target.value);
              }}/> </label>
              </div>
              <div className="mb-3">
              <label className='form-label'>Cargo: <input type="text" className='form-control' value={cargo} onChange={(event)=>{
                setCargo(event.target.value);
              }}/> </label>
              </div>
              {
                editar?
                <div>
                <button className='btn btn-warning' onClick={update}>Actualizar</button>
                <button className='btn btn-info' onClick={limpiarCampos}>Cancelar</button>
                </div>
                :<button className='btn btn-success' onClick={add}>Guardar</button>
              }
              
            </div>
          </div>
        </div>
        <br />
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Nombres</th>
              <th scope="col">Edad</th>
              <th scope="col">Pais</th>
              <th scope="col">Cargo</th>
              <th scope="col">Operaciones</th>
            </tr>
          </thead>
          <tbody>
            {
              empleadosList.map((val,key)=>{
                return <tr>
                          <td>{val.nombre}</td>
                          <td>{val.edad}</td>
                          <td>{val.pais}</td>
                          <td>{val.cargo}</td>
                          <td>
                            <button className='btn btn-danger' onClick={()=>{
                              editarEmpleado(val);
                            }}>Editar</button>
                            <button className='btn btn-warning' onClick={()=>{
                              eliminar(val.id);
                            }}>Eliminar</button>
                          </td>
                        </tr>
              })
            }
          </tbody>
        </table>
        
    </div>
    </div>
  );
}

export default App;
